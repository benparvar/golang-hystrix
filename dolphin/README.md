** Install

$ go get github.com/afex/hystrix-go/hystrix

** Usage

* run mobile adapter with hystrix

$ go run main.go

* dolphin

$ cd dolphin
$ go run main.go

** Accessing the mobile adapter

$ for i in $(seq 50); do curl -x '' localhost:8080 &; done

